<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"><title>Vote for Car</title>
</head>
<body>
<h1>Vote for Car</h1>
@foreach ($selectedCar as $car)
    <div>
        <img src="{{ url('storage/' . $car->Image) }}" alt="Car Image">
        <form action="{{ route('voteCar') }}" method="post">
            @csrf
            <input type="hidden" name="selected_model" value="{{ $car->Model }}">
            <button type="submit">Vote for this Car</button>
        </form>
    </div>
@endforeach
</body>
</html>
