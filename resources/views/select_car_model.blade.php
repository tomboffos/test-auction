<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"><title>Select Car Model</title>
</head>
<body>
<h1>Select Car Model</h1>
<form action="{{ route('voteCar') }}" method="post">
    @csrf
    <select name="selected_model">
        @foreach ($carModels as $model)
            <option value="{{ $model }}">{{ $model }}</option>
        @endforeach
    </select>
    <button type="submit">Vote</button>
</form>
</body>
</html>
