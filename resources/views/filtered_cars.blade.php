<!DOCTYPE html>
<html lang="el">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"><title>Filtered Cars</title>
</head>
<body>
<h1>Filtered Cars</h1>
<form action="{{ route('filterCars') }}" method="get">
    <select name="selected_model">
            <option value="all">All</option>
        @foreach ($filteredCars as $model)
            <option value="{{ $model->Model }}">{{ $model->Model }}</option>
        @endforeach
    </select>
    <input type="text" name="from_year" placeholder="From Year">
    <input type="text" name="to_year" placeholder="To Year">
    <button type="submit">Apply Filter</button>
</form>

<table>
    <thead>
    <tr>
        <th>Model</th>
        <th>Year</th>
        <th>Votes</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($filteredCars as $car)
        <tr>
            <td>{{ $car->Model }}</td>
            <td>{{ $car->Year }}</td>
            <td>{{ $car->Votes }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<p>Total Votes for {{ $selectedModel }} Cars in the Selected Year Range: {{ $totalVotes }}</p>
</body>
</html>
