<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('AuctionItemId');
            $table->string('AuctionId');
            $table->string('CurrentHighPreBid');
            $table->string('CustomStatus');
            $table->string('MyPreBid');
            $table->string('IsWatched');
            $table->string('SequenceNumber');
            $table->string('LastCustomStatusSetAt');
            $table->string('Year');
            $table->string('Make');
            $table->string('Model');
            $table->string('Odometer');
            $table->string('Units');
            $table->string('VehicleLocation');
            $table->string('Engine');
            $table->string('Transmission');
            $table->string('Color');
            $table->string('Brand');
            $table->string('ExternalAuctionItemId');
            $table->string('WinningBidAmount');
            $table->string('WinningBidLocation')->nullable();

            $table->string('IsBiddable');
            $table->string('Status');
            $table->string('Image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
