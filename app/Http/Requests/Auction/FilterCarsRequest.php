<?php

namespace App\Http\Requests\Auction;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $selected_model
 * @property mixed $from_year
 * @property mixed $to_year
 */
class FilterCarsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'selected_model' => 'nullable',
            'from_year' => 'nullable',
            'to_year' => 'nullable',
        ];
    }
}
