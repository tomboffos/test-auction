<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auction\FilterCarsRequest;
use App\Http\Requests\Auction\SelectModelRequest;
use App\Models\Car;
use App\Repositories\Interfaces\AuctionRepositoryInterface;

class AuctionController extends Controller
{
    protected $auctionRepository;

    public function __construct(AuctionRepositoryInterface $auctionRepository)
    {
        $this->auctionRepository = $auctionRepository;
    }

    public function selectCarModel()
    {
        $carModels = $this->auctionRepository->selectCarModel();
        return view('select_car_model', compact('carModels'));
    }

    public function voteCar(SelectModelRequest $request)
    {
        $selectedCar = $this->auctionRepository->voteCar($request);

        return view('vote_car', compact('selectedCar'));
    }

    public function filterCars(FilterCarsRequest $request)
    {
        $loadedFilteredCars = $this->auctionRepository->filterCars($request);

        return view('filtered_cars', [
            'selectedModel' => $loadedFilteredCars->selectedModel,
            'filteredCars' => $loadedFilteredCars->filteredCars,
            'totalVotes' => $loadedFilteredCars->totalVotes,
        ]);
    }

}
