<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Collection;

class FilterCarEntity
{
    public function __construct(
        public readonly ?string $selectedModel,
        public readonly ?Collection $filteredCars,
        public readonly ?int $totalVotes

    )
    {

    }
}
