<?php

namespace App\Console\Commands;

use App\Models\Car;
use Illuminate\Console\Command;

class ImportCarData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:car-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // TODO: add queue

        $jsonFilesPath = storage_path('app/public');
        $jsonFiles = scandir($jsonFilesPath);

        foreach ($jsonFiles as $jsonFile) {
            if (pathinfo($jsonFile, PATHINFO_EXTENSION) === 'json') {
                $jsonData = file_get_contents($jsonFilesPath . '/' . $jsonFile);
                $data = json_decode($jsonData, true);

                Car::create($data);

                $this->info('Imported data from ' . $jsonFile);
            }
        }

        $this->info('Car data imported successfully.');

        return 0;
    }

}
