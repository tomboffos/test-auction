<?php

namespace App\Repositories\Auction;

use App\Http\Entities\FilterCarEntity;
use App\Http\Requests\Auction\FilterCarsRequest;
use App\Http\Requests\Auction\SelectModelRequest;
use App\Http\Resources\Auction\CarResource;
use App\Models\Car;
use App\Repositories\Interfaces\AuctionRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class AuctionRepositoryImpl implements AuctionRepositoryInterface
{
    public function selectCarModel(): Collection
    {
        return Car::query()->pluck('Model');
    }

    public function voteCar(SelectModelRequest $request): Collection
    {
        if (!empty($request->selected_model)) {
            return Car::query()->where('Model', $request->selected_model)->inRandomOrder()->take(2)->get();
        } else {
            return Car::query()->get();
        }
    }

    public function filterCars(FilterCarsRequest $request) : FilterCarEntity
    {
        $selectedModel = $request->selected_model;
        $fromYear = $request->from_year;
        $toYear = $request->to_year;
        $filteredCars = Car::query()
            ->when($selectedModel !== 'all' && $selectedModel, function ($q) use ($selectedModel) {
                $q->where('Model', $selectedModel);
            })
            ->when($fromYear || $toYear, function ($q) use ($fromYear, $toYear) {
                $q->whereBetween('Year', [$fromYear ?? 1900, $toYear ?? 3000]);
            })
            ->get();

        $totalVotes = $filteredCars->sum('Votes');

        return new FilterCarEntity($selectedModel, $filteredCars, $totalVotes);
    }
}
