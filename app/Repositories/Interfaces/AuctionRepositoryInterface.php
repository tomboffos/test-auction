<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\Auction\FilterCarsRequest;
use App\Http\Requests\Auction\SelectModelRequest;

interface AuctionRepositoryInterface
{
    public function selectCarModel();

    public function voteCar(SelectModelRequest $request);

    public function filterCars(FilterCarsRequest $request);


}
