<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Car
 *
 * @property int $id
 * @property string $AuctionItemId
 * @property string $AuctionId
 * @property string $CurrentHighPreBid
 * @property string $CustomStatus
 * @property string $MyPreBid
 * @property bool $IsWatched
 * @property int $SequenceNumber
 * @property string $LastCustomStatusSetAt
 * @property int $Year
 * @property string $Make
 * @property string $Model
 * @property int $Odometer
 * @property string $Units
 * @property string $VehicleLocation
 * @property string $Engine
 * @property string $Transmission
 * @property string $Color
 * @property string $Brand
 * @property string $ExternalAuctionItemId
 * @property int $WinningBidAmount
 * @property string $WinningBidLocation
 * @property bool $IsBiddable
 * @property int $Status
 * @property string $Image
 *
 * @package App\Models
 */
class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'AuctionItemId',
        'AuctionId',
        'CurrentHighPreBid',
        'CustomStatus',
        'MyPreBid',
        'IsWatched',
        'SequenceNumber',
        'LastCustomStatusSetAt',
        'Year',
        'Make',
        'Model',
        'Odometer',
        'Units',
        'VehicleLocation',
        'Engine',
        'Transmission',
        'Color',
        'Brand',
        'ExternalAuctionItemId',
        'WinningBidAmount',
        'WinningBidLocation',
        'IsBiddable',
        'Status',
        'Image',
    ];
}
