<?php

namespace App\Providers;

use App\Repositories\Auction\AuctionRepositoryImpl;
use App\Repositories\Interfaces\AuctionRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuctionRepositoryInterface::class, AuctionRepositoryImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
